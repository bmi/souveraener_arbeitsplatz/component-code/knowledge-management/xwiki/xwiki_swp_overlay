<?xml version="1.0" encoding="UTF-8"?>

<!--
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>
  <parent>
    <groupId>com.xwiki.projects.swp</groupId>
    <artifactId>xwiki-swp-distribution</artifactId>
    <version>0.24-SNAPSHOT</version>
  </parent>
  <artifactId>xwiki-swp-distribution-war</artifactId>
  <name>XWiki Projects - Sovereign Workplace Project - Distribution - WAR</name>
  <packaging>war</packaging>
  <description>The SWP XWiki WAR package. It contains everything needed to run XWiki in production.</description>
  <properties>
    <!-- Put the name of the distribution which will be displayed in the footer -->
    <xwiki.extension.name>OpenDesk - XWiki</xwiki.extension.name>
    <xwiki.extension.features>${platform.distribution.features}</xwiki.extension.features>
    <!-- Make sure that we are including the legacy modules -->
    <war.dependencies.artifactId>${war.dependencies.artifactId.legacy}</war.dependencies.artifactId>
  </properties>
  <dependencies>
    <dependency>
      <groupId>org.xwiki.contrib.wikiinitializer</groupId>
      <artifactId>application-wiki-initializer-api</artifactId>
      <version>1.2.3</version>
      <exclusions>
        <exclusion>
          <groupId>org.xwiki.platform</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.xwiki.commons</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.xwiki.rendering</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>${project.groupId}</groupId>
      <artifactId>xwiki-swp-extension-license-initializer-initialization</artifactId>
      <version>${project.version}</version>
      <exclusions>
        <exclusion>
          <groupId>org.xwiki.platform</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.xwiki.commons</groupId>
          <artifactId>*</artifactId>
        </exclusion>
        <exclusion>
          <groupId>org.xwiki.rendering</groupId>
          <artifactId>*</artifactId>
        </exclusion>
      </exclusions>
    </dependency>
    <dependency>
      <groupId>org.xwiki.platform</groupId>
      <artifactId>${war.dependencies.artifactId}</artifactId>
      <version>${platform.version}</version>
      <type>pom</type>
    </dependency>
  </dependencies>
  <build>
    <plugins>
      <!-- Generate XWiki's configuration files -->
      <plugin>
        <groupId>org.xwiki.commons</groupId>
        <artifactId>xwiki-commons-tool-remote-resource-plugin</artifactId>
        <version>${commons.version}</version>
        <executions>
          <execution>
            <id>xwiki-platform-tool-configuration-resources</id>
          </execution>
        </executions>
      </plugin>
      <!-- Unpack the Skins resources -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-dependency-plugin</artifactId>
        <executions>
          <execution>
            <id>unpack-dependencies</id>
            <phase>prepare-package</phase>
            <goals>
              <goal>unpack</goal>
            </goals>
            <configuration>
              <artifactItems>
                <!-- Unpack the Flamingo skin -->
                <artifactItem>
                  <groupId>org.xwiki.platform</groupId>
                  <artifactId>xwiki-platform-flamingo-skin-resources</artifactId>
                  <version>${platform.version}</version>
                  <type>jar</type>
                  <excludes>META-INF/**</excludes>
                  <outputDirectory>${project.build.directory}/skin</outputDirectory>
                </artifactItem>
              </artifactItems>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <!-- Generate extension descriptor for each artifact of the WAR -->
      <plugin>
        <groupId>org.xwiki.commons</groupId>
        <artifactId>xwiki-commons-tool-extension-plugin</artifactId>
        <executions>
          <execution>
            <id>war</id>
            <goals>
              <goal>war</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-war-plugin</artifactId>
        <configuration>
          <dependentWarExcludes>
            <!-- Make sure our own descriptor doesn't get overwritten. -->
            META-INF/extension.xed,
            WEB-INF/hibernate.cfg.xml,
            WEB-INF/xwiki.cfg,
            WEB-INF/xwiki.properties
          </dependentWarExcludes>
          <webResources>
            <!-- Include License resources -->
            <resource>
              <directory>${project.build.directory}/maven-shared-archive-resources/META-INF</directory>
              <targetPath>META-INF</targetPath>
            </resource>
            <!-- Include Configuration files that were generated -->
            <resource>
              <directory>${project.build.directory}/maven-shared-archive-resources</directory>
              <targetPath>WEB-INF</targetPath>
              <includes>
                <include>hibernate.cfg.xml</include>
                <include>xwiki.cfg</include>
                <include>xwiki.properties</include>
              </includes>
            </resource>
            <!-- Add the skins -->
            <resource>
              <directory>${project.build.directory}/skin</directory>
              <targetPath>skins</targetPath>
              <filtering>false</filtering>
            </resource>
          </webResources>
          <!--
               - Exclude all XAR files from being placed in WEB-INF/lib. This is done by the WAR plugin which supports
                 XAR files (not our format, some other format! See https://jira.codehaus.org/browse/MWAR-281). The
                 reason we have XAR files in our dependencies is because we've added them in order to automatically
                 bundle all JAR files which are dependencies of XAR modules!
               - Exclude all META-INF directories not at the top level of the WAR. The Servlet spec mentions that
                 META-INF needs to be placed at the top and some Servlet Containers such as WildFly check this and
                 report an error if it's not the case (see https://jira.xwiki.org/browse/XWIKI-15567).
          -->
          <packagingExcludes>
            WEB-INF/extensions/*.xar,
            */**/META-INF/**
          </packagingExcludes>
        </configuration>
      </plugin>
      <!-- Ensure that some modules are not added as dependencies. -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-enforcer-plugin</artifactId>
        <executions>
          <execution>
            <id>enforce-banned-dependencies</id>
            <goals>
              <goal>enforce</goal>
            </goals>
            <configuration>
              <rules>
                <bannedDependencies>
                  <excludes>
                    <exclude>org.jmock:*</exclude>
                    <exclude>jmock:*</exclude>
                    <exclude>junit:*</exclude>
                    <exclude>org.junit.*:*</exclude>
                    <exclude>org.mockito:*</exclude>
                    <exclude>javax.servlet:servlet-api:*:*:compile</exclude>
                    <exclude>jakarta.mail:jakarta.mail-api</exclude>
                  </excludes>
                  <includes>
                    <!-- Allow only JUnit and JMock with scope test because -->
                    <include>org.jmock:*:*:*:test</include>
                    <include>jmock:*:*:*:test</include>
                    <include>junit:*:*:*:test</include>
                    <include>org.junit.*:*:*:*:test</include>
                    <include>org.mockito:*:*:*:test</include>
                  </includes>
                </bannedDependencies>
              </rules>
            </configuration>
          </execution>
        </executions>
      </plugin>
      <!-- Ensure we don't have duplicates in WEB-INF/lib -->
      <plugin>
        <groupId>org.basepom.maven</groupId>
        <artifactId>duplicate-finder-maven-plugin</artifactId>
        <executions>
          <execution>
            <id>war-check-duplicates</id>
          </execution>
        </executions>
      </plugin>
    </plugins>
  </build>
</project>
