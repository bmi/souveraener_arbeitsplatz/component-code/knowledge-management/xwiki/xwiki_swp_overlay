/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.auth.internal;

import java.util.HashMap;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.text.StringSubstitutor;
import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.contrib.oidc.auth.internal.OIDCClientConfiguration;
import org.xwiki.contrib.oidc.auth.internal.OIDCStringLookup;
import org.xwiki.contrib.oidc.auth.internal.OIDCUserManager;
import org.xwiki.contrib.oidc.auth.store.OIDCUserStore;

import com.nimbusds.oauth2.sdk.token.AccessToken;
import com.nimbusds.oauth2.sdk.token.BearerAccessToken;
import com.nimbusds.openid.connect.sdk.claims.UserInfo;
import com.xpn.xwiki.doc.XWikiDocument;
import com.xpn.xwiki.user.api.XWikiUser;

/**
 * Authentication service that will verify the validity of a bearer access token against the IDP.
 *
 * @version $Id$
 * @since 0.23
 */
@Component(roles = KeycloakAuthService.class)
@Singleton
public class KeycloakAuthService
{
    private static final String OIDC_PREFERRED_USERNAME = "oidc.user.preferredUsername";

    @Inject
    private OIDCUserManager oidcUserManager;

    @Inject
    private OIDCUserStore oidcUserStore;

    @Inject
    private OIDCClientConfiguration configuration;

    @Inject
    private Logger logger;

    /**
     * Check authorization of a bearer access token and returns the corresponding XWiki user if it exists.
     *
     * @param authorization the bearer access token to check
     * @return the corresponding XWiki user
     */
    public XWikiUser checkAuth(String authorization)
    {
        try {
            if (StringUtils.isBlank(authorization)) {
                logger.debug("No authorization header found. Skipping Keycloak Access Token authentication.");
                return null;
            }

            if (!StringUtils.startsWith(authorization.toLowerCase(), "bearer")) {
                logger.debug("Only Bearer tokens are accepted. Skipping Keycloak Access Token authentication.");
                return null;
            }

            String trimmedAuthorization = authorization.substring(6).trim();
            // Forge a bearer access token based on the authorization string
            AccessToken accessToken = new BearerAccessToken(trimmedAuthorization);

            // Get the user info endpoint
            UserInfo userInfo = oidcUserManager.getUserInfo(accessToken);

            // We will need to apply
            Map<String, String> formatMap = createFormatMap(userInfo);
            logger.debug("Format map: [{}]", formatMap);
            StringSubstitutor substitutor = new StringSubstitutor(new OIDCStringLookup(formatMap));
            String subject = substitutor.replace(this.configuration.getSubjectFormater());

            // Search for the user. We consider so far that the user document already exists in the wiki.
            if (logger.isDebugEnabled()) {
                logger.debug("Searching for corresponding user document with issuer [{}] and subject [{}]",
                    configuration.getIssuer().getValue(), subject);
            }
            XWikiDocument userDocument = oidcUserStore.searchDocument(configuration.getIssuer().getValue(), subject);

            if (userDocument != null) {
                return new XWikiUser(userDocument.getDocumentReference());
            }
        } catch (Exception e) {
            logger.error("Failed to check user authentication with bearer token", e);
        }

        return null;
    }

    /**
     * Simpler implementation of OIDCUserManager#createFormatMap(IDTokenClaimsSet, UserInfo), that keeps only things
     * related to OIDC User info.
     *
     * @param userInfo the user info claims
     * @return the format map.
     */
    private Map<String, String> createFormatMap(UserInfo userInfo)
    {
        Map<String, String> formatMap = new HashMap<>();

        // User information
        putVariable(formatMap, "oidc.user.subject", userInfo.getSubject().getValue());
        if (userInfo.getPreferredUsername() != null) {
            putVariable(formatMap, OIDC_PREFERRED_USERNAME, userInfo.getPreferredUsername());
        } else {
            putVariable(formatMap, OIDC_PREFERRED_USERNAME, userInfo.getSubject().getValue());
        }
        putVariable(formatMap, "oidc.user.mail", userInfo.getEmailAddress() == null ? "" : userInfo.getEmailAddress());
        putVariable(formatMap, "oidc.user.familyName", userInfo.getFamilyName());
        putVariable(formatMap, "oidc.user.givenName", userInfo.getGivenName());

        // Inject the whole JSON
        addJSON("oidc.user.", userInfo.toJSONObject(), formatMap);
        return formatMap;
    }

    private String clean(String str)
    {
        return RegExUtils.removePattern(str, "[\\.\\:\\s,@\\^]");
    }

    private void putVariable(Map<String, String> map, String key, String value)
    {
        if (value != null) {
            map.put(key, value);

            map.put(key + ".lowerCase", value.toLowerCase());
            map.put(key + "._lowerCase", value.toLowerCase());
            map.put(key + ".upperCase", value.toUpperCase());
            map.put(key + "._upperCase", value.toUpperCase());

            String cleanValue = clean(value);
            map.put(key + ".clean", cleanValue);
            map.put(key + "._clean", cleanValue);
            map.put(key + ".clean.lowerCase", cleanValue.toLowerCase());
            map.put(key + "._clean._lowerCase", cleanValue.toLowerCase());
            map.put(key + ".clean.upperCase", cleanValue.toUpperCase());
            map.put(key + "._clean._upperCase", cleanValue.toUpperCase());
        }
    }

    private void addJSON(String prefix, Map<String, ?> json, Map<String, String> formatMap)
    {
        for (Map.Entry<String, ?> entry : json.entrySet()) {
            if (entry.getValue() != null) {
                if (entry.getValue() instanceof Map) {
                    addJSON(prefix + entry.getKey() + '.', (Map) entry.getValue(), formatMap);
                } else {
                    putVariable(formatMap, prefix + entry.getKey(), entry.getValue().toString());
                }
            }
        }
    }
}
