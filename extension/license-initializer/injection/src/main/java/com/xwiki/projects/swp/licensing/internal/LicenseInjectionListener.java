/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.licensing.internal;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;

import org.slf4j.Logger;
import org.xwiki.bridge.event.ApplicationReadyEvent;
import org.xwiki.component.annotation.Component;
import org.xwiki.observation.AbstractEventListener;
import org.xwiki.observation.event.Event;

import com.xwiki.licensing.License;
import com.xwiki.licensing.LicenseManager;

/**
 * Listener that will inejct licenses extracted earlie in the startup process into the licensor.
 * We need the application to be fully ready before injecting these licenses in order to make sure that new
 * licenses are correctly persisted.
 *
 * @version $Id$
 * @since 0.21
 */
@Component
@Singleton
@Named(LicenseInjectionListener.LISTENER_NAME)
public class LicenseInjectionListener extends AbstractEventListener
{
    /**
     * The listener name.
     */
    public static final String LISTENER_NAME = "licenseInjectionListener";

    @Inject
    private LicenseInitializerConfiguration licenseInitializerConfiguration;

    @Inject
    private LicenseManager licenseManager;

    @Inject
    private Logger logger;

    /**
     * Default constructor.
     */
    public LicenseInjectionListener()
    {
        super(LISTENER_NAME, new ApplicationReadyEvent());
    }

    @Override
    public void onEvent(Event event, Object source, Object data)
    {
        for (License license : licenseInitializerConfiguration.getLicenses()) {
            if (licenseManager.add(license)) {
                logger.info("License [{}] has been added to the licensor.", license);
            } else {
                logger.info("License [{}] has not been added to the licensor.", license);
            }
        }
    }
}
