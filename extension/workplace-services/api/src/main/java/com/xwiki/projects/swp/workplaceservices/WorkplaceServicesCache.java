/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices;

import java.util.List;
import java.util.Locale;

import org.xwiki.component.annotation.Role;
import org.xwiki.model.reference.DocumentReference;

/**
 * Cache for the workplace services.
 *
 * @version $Id$
 * @since 1.0
 */
@Role
public interface WorkplaceServicesCache
{
    /**
     * Get the categories associated to the given user.
     *
     * @param userReference the user for which the categories should be fetched
     * @param locale the locale used by the user
     * @return the available categories
     */
    List<WorkplaceServiceCategory> getCategories(DocumentReference userReference, Locale locale);

    /**
     * Get the categories associated to the guest user.
     *
     * @param locale the locale used by the user
     * @return the available categories
     */
    List<WorkplaceServiceCategory> getGuestCategories(Locale locale);

    /**
     * Set the list of available workplace services to the given user.
     *
     * @param userReference the user to which the services should be set
     * @param locale the locale used by the user
     * @param categories the categories to assign
     */
    void setCategories(DocumentReference userReference, Locale locale, List<WorkplaceServiceCategory> categories);

    /**
     * Set the list of available workspace services for the guest user.
     *
     * @param locale the locale used by the user
     * @param categories the categories to assign
     */
    void setGuestCategories(Locale locale, List<WorkplaceServiceCategory> categories);
}
