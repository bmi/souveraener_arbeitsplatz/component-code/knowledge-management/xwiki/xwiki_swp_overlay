/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.xwiki.cache.Cache;
import org.xwiki.cache.CacheException;
import org.xwiki.cache.CacheManager;
import org.xwiki.cache.config.CacheConfiguration;
import org.xwiki.cache.eviction.LRUEvictionConfiguration;
import org.xwiki.component.annotation.Component;
import org.xwiki.component.manager.ComponentLifecycleException;
import org.xwiki.component.phase.Disposable;
import org.xwiki.component.phase.Initializable;
import org.xwiki.component.phase.InitializationException;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.EntityReferenceSerializer;

import com.xwiki.projects.swp.workplaceservices.WorkplaceServiceCategory;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesCache;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesConfiguration;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesException;

/**
 * Default implementation for the {@link WorkplaceServicesCache}.
 *
 * @version $Id$
 * @since 1.0
 */
@Component
@Singleton
public class DefaultWorkplaceServicesCache implements WorkplaceServicesCache, Initializable, Disposable
{
    private static final String CACHE_NAME = "cache.workplaceServices";

    private static final String GUEST_CATEGORIES_KEY = "XWIKI_GUEST";

    @Inject
    private CacheManager cacheManager;

    @Inject
    private EntityReferenceSerializer<String> documentReferenceSerializer;

    @Inject
    private WorkplaceServicesConfiguration workplaceServicesConfiguration;

    private Cache<List<WorkplaceServiceCategory>> cache;

    @Override
    public void initialize() throws InitializationException
    {
        try {
            CacheConfiguration cacheConfiguration = new CacheConfiguration();
            cacheConfiguration.setConfigurationId(CACHE_NAME);
            LRUEvictionConfiguration lru = new LRUEvictionConfiguration();
            lru.setMaxEntries(workplaceServicesConfiguration.getServiceCacheMaxEntries());
            lru.setLifespan(workplaceServicesConfiguration.getServiceCacheEntryLifespan());
            cacheConfiguration.put(LRUEvictionConfiguration.CONFIGURATIONID, lru);
            this.cache = this.cacheManager.createNewCache(cacheConfiguration);
        } catch (CacheException | WorkplaceServicesException e) {
            throw new InitializationException("Failed to initialize Workplace Services cache", e);
        }
    }

    @Override
    public void dispose() throws ComponentLifecycleException
    {
        if (this.cache != null) {
            this.cache.dispose();
        }
    }

    @Override
    public List<WorkplaceServiceCategory> getCategories(DocumentReference userReference, Locale locale)
    {
        return this.cache.get(computeCacheEntryName(userReference, locale));
    }

    @Override
    public List<WorkplaceServiceCategory> getGuestCategories(Locale locale)
    {
        return this.cache.get(computeCacheEntryName(GUEST_CATEGORIES_KEY, locale));
    }

    @Override
    public void setCategories(DocumentReference userReference, Locale locale, List<WorkplaceServiceCategory> categories)
    {
        this.cache.set(computeCacheEntryName(userReference, locale), categories);
    }

    @Override
    public void setGuestCategories(Locale locale, List<WorkplaceServiceCategory> categories)
    {
        this.cache.set(computeCacheEntryName(GUEST_CATEGORIES_KEY, locale), categories);
    }

    private String computeCacheEntryName(DocumentReference userReference, Locale locale)
    {
        return computeCacheEntryName(documentReferenceSerializer.serialize(userReference), locale);
    }

    private String computeCacheEntryName(String prefix, Locale locale)
    {
        return String.format("%s-%s", prefix, locale.toLanguageTag());
    }
}
