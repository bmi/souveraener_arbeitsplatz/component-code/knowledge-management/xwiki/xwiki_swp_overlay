/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.net.MalformedURLException;
import java.net.URL;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.apache.commons.lang3.StringUtils;
import org.xwiki.component.annotation.Component;
import org.xwiki.configuration.ConfigurationSource;

import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesConfiguration;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesException;

/**
 * Default implementation of {@link WorkplaceServicesConfiguration}.
 *
 * @version $Id$
 * @since 1.0
 */
@Component
@Singleton
public class DefaultWorkplaceServicesConfiguration implements WorkplaceServicesConfiguration
{
    private static final String CONFIGURATION_PREFIX = "workplaceServices.";

    private static final String CACHE_PREFIX = "cache.";

    @Inject
    private ConfigurationSource configurationSource;

    @Override
    public URL getNavigationEndpoint() throws WorkplaceServicesException
    {
        String navigationEndpoint = configurationSource.getProperty(CONFIGURATION_PREFIX + "navigationEndpoint",
            StringUtils.EMPTY);

        try {
            return new URL(navigationEndpoint);
        } catch (MalformedURLException e) {
            throw new WorkplaceServicesException("Failed to create URL for the navigation endpoint", e);
        }
    }

    @Override
    public URL getBase() throws WorkplaceServicesException
    {
        String workplaceBase = configurationSource.getProperty(CONFIGURATION_PREFIX + "base",
            StringUtils.EMPTY);

        try {
            return new URL(workplaceBase);
        } catch (MalformedURLException e) {
            throw new WorkplaceServicesException("Failed to create URL for the workplace base", e);
        }
    }

    @Override
    public String getPortalSecret() throws WorkplaceServicesException
    {
        return configurationSource.getProperty(CONFIGURATION_PREFIX + "portalSecret");
    }

    @Override
    public int getServiceCacheMaxEntries() throws WorkplaceServicesException
    {
        return configurationSource.getProperty(CONFIGURATION_PREFIX + CACHE_PREFIX + "maxEntries", 1000);
    }

    @Override
    public int getServiceCacheEntryLifespan() throws WorkplaceServicesException
    {
        return configurationSource.getProperty(CONFIGURATION_PREFIX + CACHE_PREFIX + "lifespan", 1800);
    }
}
