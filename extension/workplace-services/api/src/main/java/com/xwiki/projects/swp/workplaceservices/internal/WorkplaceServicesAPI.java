/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.inject.Provider;
import javax.inject.Singleton;

import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthenticationException;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.BasicHttpContext;
import org.slf4j.Logger;
import org.xwiki.component.annotation.Component;
import org.xwiki.model.reference.DocumentReference;

import com.xpn.xwiki.XWiki;
import com.xpn.xwiki.XWikiContext;
import com.xpn.xwiki.XWikiException;
import com.xpn.xwiki.doc.XWikiDocument;
import com.xpn.xwiki.objects.BaseObject;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServiceCategory;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesConfiguration;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesException;

/**
 * Component responsible for fetching workplaces services from the Workplace Services API.
 *
 * @version $Id$
 * @since 1.0
 */
@Component(roles = WorkplaceServicesAPI.class)
@Singleton
public class WorkplaceServicesAPI
{
    private static final DocumentReference OIDC_CLASS =
        new DocumentReference(XWiki.DEFAULT_MAIN_WIKI, Arrays.asList(XWiki.SYSTEM_SPACE, "OIDC"), "UserClass");

    private static final String SUBJECT = "subject";

    @Inject
    private WorkplaceServicesConfiguration workplaceServicesConfiguration;

    @Inject
    private Provider<XWikiContext> contextProvider;

    @Inject
    private Logger logger;

    /**
     * Fetches categories from the Workplace Services API.
     *
     * @param userReference the user to use for fetching caterogies
     * @param locale the locale used in the interface
     * @return the workplace services category
     * @throws WorkplaceServicesException if an error happened
     */
    public List<WorkplaceServiceCategory> fetchCategories(DocumentReference userReference, Locale locale)
        throws WorkplaceServicesException
    {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            URI navigationURI = new URIBuilder(workplaceServicesConfiguration.getNavigationEndpoint().toURI())
                .addParameter("language", locale.toLanguageTag())
                .addParameter("base", workplaceServicesConfiguration.getBase().toString())
                .build();
            logger.debug("Fetching workplace services with URI [{}]", navigationURI);

            HttpGet httpGet = new HttpGet(navigationURI);
            if (userReference != null) {
                httpGet.addHeader(
                    new BasicScheme().authenticate(
                        new UsernamePasswordCredentials(
                            getSWPUsername(userReference),
                            workplaceServicesConfiguration.getPortalSecret()), httpGet, new BasicHttpContext()));
            }

            HttpResponse response = httpClient.execute(httpGet);

            if (response.getStatusLine().getStatusCode() == 200) {
                String rawCategories = new String(response.getEntity().getContent().readAllBytes());
                logger.debug("Parsing categories based on API response [{}]", rawCategories);
                return WorkplaceServicesJSONParser.parseCategories(rawCategories, logger);
            } else {
                throw new WorkplaceServicesException(String.format(
                    "Failed to get a server response for the workplace services for user [%s]", userReference));
            }
        } catch (URISyntaxException | AuthenticationException | IOException e) {
            throw new WorkplaceServicesException(
                String.format("Failed to get categories for user [%s]", userReference), e);
        }
    }

    private String getSWPUsername(DocumentReference userReference)
    {
        // Load the document and check the OIDC Object of the user
        XWikiContext xWikiContext = contextProvider.get();
        XWiki xWiki = xWikiContext.getWiki();
        try {
            XWikiDocument userDocument = xWiki.getDocument(userReference, xWikiContext);
            BaseObject oidcObject = userDocument.getXObject(OIDC_CLASS);
            if (oidcObject != null
                && org.apache.commons.lang3.StringUtils.isNotBlank(oidcObject.getStringValue(SUBJECT))) {
                String[] splitOIDCSubject = oidcObject.getStringValue(SUBJECT).split(":");
                return splitOIDCSubject[splitOIDCSubject.length - 1];
            } else {
                return userReference.getName();
            }
        } catch (XWikiException e) {
            return userReference.getName();
        }
    }
}
