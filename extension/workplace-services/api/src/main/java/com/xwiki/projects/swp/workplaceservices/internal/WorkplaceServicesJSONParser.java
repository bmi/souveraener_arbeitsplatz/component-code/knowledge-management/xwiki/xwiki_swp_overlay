/*
 * See the NOTICE file distributed with this work for additional
 * information regarding copyright ownership.
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of
 * the License, or (at your option) any later version.
 *
 * This software is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this software; if not, write to the Free
 * Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA
 * 02110-1301 USA, or see the FSF site: http://www.fsf.org.
 */
package com.xwiki.projects.swp.workplaceservices.internal;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import org.slf4j.Logger;
import org.xwiki.text.StringUtils;

import com.xwiki.projects.swp.workplaceservices.WorkplaceServiceCategory;
import com.xwiki.projects.swp.workplaceservices.WorkplaceServicesEntry;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

/**
 * Helper to parse JSON response form the workplace services API.
 *
 * @version $Id$
 * @since 1.0
 */
public final class WorkplaceServicesJSONParser
{
    private static final String DISPLAY_NAME = "display_name";

    private static final String IDENTIFIER = "identifier";

    private static final String ICON_URL = "icon_url";

    private static final String LINK = "link";

    private WorkplaceServicesJSONParser()
    {

    }

    /**
     * Convert a given JSON payload into a list of workplace services.
     *
     * @param content the payload
     * @param logger the logger
     * @return the parsed categories
     * @throws MalformedURLException if one of the categories has a bad icon or access URL
     */
    public static List<WorkplaceServiceCategory> parseCategories(String content, Logger logger)
        throws MalformedURLException
    {
        // Parse the response
        JSONObject jsonContent = JSONObject.fromObject(content);
        JSONArray jsonCategories = jsonContent.getJSONArray("categories");

        List<WorkplaceServiceCategory> categories = new ArrayList<>();
        for (ListIterator categoryIt = jsonCategories.listIterator(); categoryIt.hasNext();) {
            JSONObject jsonCategory = (JSONObject) categoryIt.next();

            // Resolve each service in the category
            JSONArray jsonEntries = jsonCategory.getJSONArray("entries");
            List<WorkplaceServicesEntry> entries = new ArrayList<>();
            for (ListIterator entryIt = jsonEntries.listIterator(); entryIt.hasNext();) {
                JSONObject jsonEntry = (JSONObject) entryIt.next();
                URL iconURL = null;
                URL linkURL = null;

                try {
                    if (jsonEntry.containsKey(ICON_URL) && StringUtils.isNotBlank(jsonEntry.getString(ICON_URL))) {
                        iconURL = new URL(jsonEntry.getString(ICON_URL));
                    }
                    if (jsonEntry.containsKey(LINK) && StringUtils.isNotBlank(jsonEntry.getString(LINK))) {
                        linkURL = new URL(jsonEntry.getString(LINK));
                    }
                } catch (MalformedURLException e) {
                    logger.warn("Found malformed URL in JSON Object [{}]", jsonEntry);
                }

                WorkplaceServicesEntry entry = new DefaultWorkplaceServicesEntry(
                    jsonEntry.getString(IDENTIFIER), jsonEntry.getString(DISPLAY_NAME),
                    iconURL, linkURL, jsonEntry.getString("target")
                );
                entries.add(entry);
            }

            WorkplaceServiceCategory category = new DefaultWorkplaceServicesCategory(
                jsonCategory.getString(IDENTIFIER),
                jsonCategory.getString(DISPLAY_NAME),
                entries
            );
            categories.add(category);
        }

        return categories;
    }
}
